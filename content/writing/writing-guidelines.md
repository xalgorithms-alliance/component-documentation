---
title: 'Writing Guidelines'
order: 0
---

[//]: # (you can learn about markdown syntax here https://www.markdownguide.org/basic-syntax)

[//]: # (You can write comments like this, and they will be visible to editors, but will not be rendered on the page)

### Writing Comes from the Community

As an open source project, not only does code and documentation come from the community, so does our communications strategy, and web content. This document is intended to provide rough guidlines for those interested in contributing writing.

### The Philosophy Informing Writing

The philosophy behind the Xalgorithms Foundation's communications strategy is that Oughtomation's benefits are best expressed by highlighting the multiple voices and perspectives involved in Xalgorithms Foundation collaborations. 




### Terms

Distinguishing oughtomation from artificial intelligence (AI) - Oftentimes rule makers and rules takers conflate algorithmic policy with the automation of legal enforcement. This may then lead to the further association of artificial intelligence for automated compliance (in other words, ‘autocratic’)  systems  whether hosted in the physical or digital realm. Our human-centred approach emphasizes the need for human-readable expression, expectation-setting, and the anticipation of error in ‘oughtomation’. Our approach is rooted in maintaining end-user agency and artificial naïvety, rather than rule-maker autocracy and artificial intelligence.

Emphasizing the importance of tolerance in system design - The designer of a system is responsible for considering the potential interface, experience, and prerogative of its users in their totality. Without considering the user’s own experience and own prerogatives, the designer of a system may be responsible for a convoluted interpretation of the system and may not place adequate significance upon an end-user's autonomous perception of how it is or is not useful to them. This may interfere with practical experimentation, implementation and adoption.

Highlighting the benefits of interoperability -To truly enable end-user agency in ‘oughtomation’ it is critical that individuals in the public and private sector are empowered to develop solutions/applications that carry out the method without stringent restrictions in the oughtomation specification as to how. Private and public sector initiatives often create silos that lead to competition rather than cooperation. ‘Co-opetition’ through interoperability enables greater market access and user-choice in an evermore interconnected world. It is essential for equitable interaction between rule makers and rule takers in digital systems. With the rise in development and adoption of a variety of distributed systems protocols for myriad use-cases, it is vital that ‘oughtomation’ comes to be understood as not a replacement for, but an auxiliary capability to enhance any other systems, networks, and applications. 

Market demand and social preference for transparency and simplicity - Technological innovation often leaves its adopters in the dark about the inner workings, restrictions and/or issues that they entail for society. Principles inherent in free/libre/open licensing and methods have long been driven by market demand and social preference for greater transparency and simplicity in critical system design. Demand for greater transparency will bring competition among designers and compel them to embody simplicity as a core principle to ensure end-user agency. ‘Oughtomation’ as a method embodies simplicity as a core function of design, and thus must also demonstrate such in its communication to its end-users and implementers. 

<h3 id="social">Social Media</h3>

[//]: # (To be developed further)

**Channels**

Twitter: [@Xalgorithms](https://twitter.com/xalgorithms) [@InternetofRules](https://twitter.com/internetofrules) [@Xalgo4Trade](https://twitter.com/xalgo4trade)

LinkedIn: [Xalgorithms Foundation](https://ca.linkedin.com/company/xalgorithms-foundation) | [Xalgorithms Alliance](https://ca.linkedin.com/company/xalgorithms)

Vimeo: [Xalgorithms Alliance](https://vimeo.com/xalgorithms)

Hashtags: #Xalgorithms #Oughtomation #InternetofRules #IoR #RuleasData #RaD #Xalgo #free #open #libre #opensource #freesoftware #distributed #decentralized #rulesautomation #computationalrules #computationallaw #practical #normative #data 

**Drafted Content** (Twitter Character Limit = 280)
**Drafted Social Media Content** (In Twitter Format; Character Limit = 280)

Category 1 - About Xalgorithms, the collaboration model and people / community

Post:
How can you create a flexible, high tolerance brand for a decentralized #FreeSoftware community?

Learn how Calvin Hutcheon (@millennialglyph) has pulled this off with the new @Xalgorithms identity! #FOSS #Libre #Design #HeadlessBrand #OpenSource

https://xalgorithms.org/writing/brand-process


"I get to write as I learn my way... for those reading, this is an invitation to join me and us."

Deja Netwon's journey @Xalgorithms began here. Check out her #blog to learn about her role on the team and other #ERA contributors. #GIS #Communications

https://www.xalgorithms.org/writing/Explaining-ERA-and-my-Role-at-Xalgorithms


Navigating #impostersyndrome, #tech, and #EarthReserve Assurance, check out Deja's insights on what it means to be #BlackinTech and her work on #ERA! #GIS #Communications

https://xalgorithms.org/writing/Coding-Imposter-Syndrome-and-Gratitude


Category 2 - About technical methods, approaches and philosophy

Post:
"The Bezier Spline itself is a useful way to draw continuous curves by using a few simple equations..."

Deja's diving headfirst into visualizing the #ERA index curve via @ProjectJupyter and @plotlygraphs.

Check out her blog on the @Xalgorithms website: https://xalgorithms.org/writing/Making-a-Bezier-Spline-and-Updates


Category 3 - About use cases, research and development

Post:
Growing up in Canada’s 🇨🇦 port city of Halifax, #trade has always been a part of @craigaatkinson’s life.

Now, with @Xalgorithms @Xalgo4Trade, he’s on a mission to advance #rules and the #Internet for #tradefacilitation / #digitaltrade. #RulesasData

https://www.xalgorithms.org/writing/How-Algorithms-Can-Enhance


Wondering how #EarthReserve Assurance (#ERA) use case work @Xalgorithms is going?

Check out Deja's updates on using #GIS to calculate normalized difference vegetation index (#NDVI) across #ecoregions: https://xalgorithms.org/writing/Using-Geospatial-Data-to-define-Ecoregions-and-Ecosystem-Integrity


Will Conboy, an undergraduate at @NorthwesternU, is working with @Xalgorithms and Northwestern's CCL to use an #economic #framework - #EarthReserveAssurance (#ERA) - that aligns both economic and #environmental incentives! #agentbasedmodelling #ABM

https://xalgorithms.org/writing/agent-based-modelling-and-the-earth-reserve-assurance







