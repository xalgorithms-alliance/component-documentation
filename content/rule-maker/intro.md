---
title: 'Intro'
order: 0
next: [{title: 'Philosophy', target: 'philosophy'}]
---

The [Oughtomation Paper](https://gitlab.com/xalgorithms-alliance/oughtomation-paper) by Joseph Potvin et al specifies an:

_"end-to-end design for human-centred digitally-assisted project management with a general-purpose request-response method, [by which], anyone will be able to publish, discover, fetch, scrutinize, prioritize and have the capability to mobilize normative data on digital networks with precision, simplicity, scale, volume and speed, and with deference to prerogatives, agreements and preferences."_

This application is one of three core reference implementations that comprise the Oughtomation Method. Rule maker(RM) is responsible for the the description, maintenance, assembly, and publication of normative data within the Oughtomation specification. To this end, Rule Maker is concerned with "the imperative prerogative arising from social or institutional spheres which empowers a person in the ‘rule maker’ role to compose statutes, contracts, specifications, policies, treaties and other relational structures among two or more parties, " Potvin writes.  Crucially, this indicates that RM plays an interfacial role, at the edge of the network, facilitating the description of institutional, social and ecological prerogative into normative data that may be transmitted across the network. This is accomplished through the authorship of logic tables, and the description of accompanying metadata. 

From this the core actions of the application can be derived. RM must facilitate the authorship of logic tables, the description of required metadata, and the publication of this data to digital networks. 