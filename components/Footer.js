import style from './Footer.module.css'
import Link from 'next/link'

export default function Footer() {


    return(
        <div>
            <div>
                <div>
                    <p>Brand</p>
                    <Link href="/brand"><a>Logo</a></Link>
                    <Link href="/brand"><a>Type</a></Link>
                    <Link href="/brand"><a>Color</a></Link>
                    <Link href="/brand"><a>Image</a></Link>
                </div>
                <div>
                    <p>Writing</p>
                    <Link href="/brand"><a>Tone</a></Link>
                    <Link href="/brand"><a>Social</a></Link>
                </div>
                <div>
                    <p>Component</p>
                    <Link href="/brand"><a>Type</a></Link>
                    <Link href="/brand"><a>Color</a></Link>
                    <Link href="/brand"><a>Icons</a></Link>
                    <Link href="/brand"><a>Buttons</a></Link>
                    <Link href="/brand"><a>Fields</a></Link>
                    <Link href="/brand"><a>Layout</a></Link>
                </div>
                <div>
                    <p>Rule Maker</p>
                    <Link href="/brand"><a>Methodology</a></Link>
                    <Link href="/brand"><a>Values</a></Link>
                    <Link href="/brand"><a>Specifications</a></Link>
                    <Link href="/brand"><a>IDE</a></Link>
                    <Link href="/brand"><a>Editor</a></Link>
                </div>
                <div>
                    <p>Contribute</p>
                   
                </div>
            </div>
            <div>
                Xalgorithms
            </div>
        </div>
    )
}