import style from './Header.module.css'
import React from 'react'
import { Grid12, Column, Button } from 'xf-material-components/package/index'

function Header({ title, description, buttonLable, target }) {
 return (
     <div className={style.hero}>
         <Grid12>
            <div className={style.headline}>
                <Column>
                    <h2>
                        {title}
                    </h2>
                    <h4>
                        {description}
                    </h4>
                    <div>
                        <a href={target} target="blank">
                            <Button>
                                {buttonLable}
                            </Button>
                        </a>
                    </div>
                </Column>
            </div>
         </Grid12>
     </div>
 )
}

export default Header