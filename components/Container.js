import React from 'react'
import PropTypes from 'prop-types'

function Container ({ children }) {
  const column = {
    maxWidth: '600px',
    margin: 'auto'
  }
  return (
        <div style={column}>{children}</div>
  )
}

Container.propTypes = {
  children: PropTypes.any
}

export default Container
