import React from 'react'
import { Grid12 } from 'xf-material-components/package/index'
import Link from 'next/link'

import style from './Navbar.module.css'

function Navbar () {
  const listyle = {
    display: 'flex',
    alignItems: 'center'
  }

  const hold = {
    width: '100%',
    padding: '1em',
    background: 'var(--slate)',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  }

  const small = {
    color: 'black',
    textDecoration: 'none',
    fontSize: '1.6em'
  }

  const linkStyle = {
    textDecoration: 'none',
    color: 'black'
  }

  return (
    <div className={style.hold}>
      <Grid12>
        <div className={style.margin}>
          <div className={style.listyle}>
            <div className={style.logoHold}>
              <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 35.24 34.71">
                  <title>logomark-plain</title>
                  <circle cx="29.68" cy="29.15" r="4.19"/>
                  <circle cx="29.68" cy="17.36" r="2.09"/>
                  <circle cx="17.62" cy="29.15" r="1.89"/>
                  <circle cx="17.62" cy="5.56" r="3.4"/>
                  <circle cx="5.56" cy="17.36" r="3.72"/>
                  <circle cx="5.56" cy="29.15" r="5.56" fill="#0f0f0f"/>
                  <circle cx="5.56" cy="5.56" r="5.56" fill="#0f0f0f"/>
                  <circle cx="29.68" cy="5.56" r="5.56" fill="#0f0f0f"/>
                  <circle cx="17.62" cy="17.36" r="5.56" fill="#0f0f0f"/>
              </svg>
            </div>
              <Link href="/"><a style={linkStyle}>Design &amp; Brand</a></Link>
          </div>
          <div className={style.flexHold}>
            <div><Link href="/brand"><a style={linkStyle}>Brand</a></Link></div>
            <div><Link href="/writing"><a style={linkStyle}>Writing</a></Link></div>
            <div><Link href="/components"><a style={linkStyle}>Components</a></Link></div>
            <div>
              <Link href="/xrm-dev">
              <a style={linkStyle}>XRM-Dev</a>
              </Link>
            </div>
            <div><Link href="/contribute"><a style={linkStyle}>Contribute</a></Link></div>
          </div>
        </div>
      </Grid12>
    </div>
  )
}

export default Navbar