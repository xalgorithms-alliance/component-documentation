import { Column, LinkButton, Icon, Button } from 'xf-material-components/package/index'
import Link from 'next/link'
import style from './XrmSidebar.module.css'

export default function xrmSidebar({prev=[], next=[], links=[]}) {
  const margin = {
    marginRight: '1em'
  }

  const padding = {
    paddingTop: '1em'
  }

  const renderLinks = () => {
    return(
      links.map(({ id, title }, index) => (
        (title === "Intro") ? ( 
          <div key={index}>
            <Link href='/xrm-dev'><a>{title}</a></Link>
          </div>
        ) : 
        ( 
          <div key={index}>
            <Link href={'/xrm-dev/' + id}><a>{title}</a></Link>
          </div>
        )
        
      ))
    )
  }

    return(
      <div className={style.hold}>
        <div>
        <h3>Intro</h3>
        <div className={style.nextflex}>
          <div>
          {prev ? ( 
          <div>
          
              <Icon name="ArrowLeft" />
              <p>{ prev.title }</p>
            
          </div>
          ) : ( 
            null 
          )}
          </div>
          <div>
          { next ? (
            <div>
            
              <Icon name="ArrowRight" />
              <p>{next.title}</p>
            
            </div>
          ) : (
            null
          )}
          </div>
        </div>
        
        
        <Column>
          {renderLinks()}
            </Column>
            </div>
              <div className={style.center}>
                <Button type="wide">
                  Repo
                </Button>
                <div className={style.flexhold}>
                  <a href="">Edit This Page</a>
                  <Icon name="External" fill="var(--primary75)"/>
                </div>
              </div>
            </div>
    )
}