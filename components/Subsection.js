import style from './Subsection.module.css'

export default function Subsection({title}) {
    return(
        <div className="subsection">
            <h5 className='counting'>{title}</h5>
        </div>
    )
}