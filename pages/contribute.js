import { getPostData, getSortedPostsData } from '../lib/components'
import PageLayout from '../components/PageLayout'
import { Column, Grid12, LinkButton } from 'xf-material-components/package/index'
import Header from '../components/Header'

export async function getStaticProps() {
  const postData = await getPostData('content/contribute', 'contribution-guidelines')
  const allPostsData = getSortedPostsData('content/contribute')
  return {
    props: {
      postData,
      allPostsData
    }
  }
}
  

export default function contribute({postData, allPostsData}) {
  
    return (
      <>
        <PageLayout posts={allPostsData}>
            <Column>
              <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} className="SetWritingHold"/>
            </Column>
        </PageLayout>
      </>
    )
  }