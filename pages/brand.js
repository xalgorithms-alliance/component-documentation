import { Column, Grid12 } from 'xf-material-components/package/index'
import { Button } from 'xf-material-components/package/index'
import Header from '../components/Header'
import { getPostData, getSortedPostsData } from '../lib/components'
import PageLayout from '../components/PageLayout'

export async function getStaticProps() {
  const postData = await getPostData('content/brand', 'intro')
  const allPostsData = getSortedPostsData('content/brand')
  return {
    props: {
      postData,
      allPostsData
    }
  }
}

function Brand ({postData, allPostsData}) {
  

  return (
    <>
     <PageLayout posts={allPostsData}>
       <div className="texthold">
            <Column>
              <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} className="SetWritingHold"/>
            </Column>
        </div>
     </PageLayout>
     
    </>
  )
}

export default Brand
