import 'xf-material-components/package/config/rootStyle.css'
import '../styles/globals.css'
import Navbar from '../components/Navbar'



function MyApp({ Component, pageProps, brand }) {
  console.log(brand)
  return (
    <>
      <Navbar />
      <Component {...pageProps} />
    </>
    )
}

export default MyApp
