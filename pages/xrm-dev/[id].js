import Head from 'next/head'
import { getAllPostIds, getPostData, getSortedPostsData } from '../../lib/components'
import { Column, Grid12 } from 'xf-material-components/package/index'
import PageLayout from '../../components/PageLayout'


export async function getStaticPaths() {
    const paths = getAllPostIds('content/rule-maker')
    return {
      paths,
      fallback: false
    }
  }

  export async function getStaticProps({ params }) {
    const postData = await getPostData('content/rule-maker', params.id)
    const allPostsData = getSortedPostsData('content/rule-maker')
    return {
      props: {
        postData,
        allPostsData
      }
    }
  }


  

  export default function Post({ postData, allPostsData }) {

    const content = {
      gridArea: '1/4/1/8',
      width: '550px',
      paddingTop: '169px'
    }

    const links = {
      position: '-webkit-sticky',
      position: 'sticky',
      top: '140px',
      height: '200px',
      gridArea: '1/2/1/4',
    }

    return (
      <div>
        <Head>
          <title>{postData.title}</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <PageLayout posts={allPostsData}>
        <div className='texthold'>
          <h2>{postData.title}</h2>
            <Column>
              <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} className="SetWritingHold"/>
            </Column>
          </div>
        </PageLayout>
      </div>
    )
  }

  