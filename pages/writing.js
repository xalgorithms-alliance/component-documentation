import { getPostData, getSortedPostsData } from '../lib/components'
import { Column, Grid12, LinkButton } from 'xf-material-components/package/index'
import Header from '../components/Header'
import PageLayout from '../components/PageLayout'

export async function getStaticProps() {
    const postData = await getPostData('content/writing', 'writing-guidelines')
    const allPostsData = getSortedPostsData('content/writing')
    return {
      props: {
        postData,
        allPostsData
      }
    }
  }

  const content = {
    gridArea: '1/4/1/8',
    width: '550px'
  }

  const links = {
    position: '-webkit-sticky',
    position: 'sticky',
    top: '140px',
    height: '200px',
    gridArea: '1/2/1/4'
  }

  const padding = {
    paddingTop: '1em'
  }

export default function writing({postData, allPostsData}) {
    return (
      <div>
        
        <PageLayout posts={allPostsData}>
          <div className="texthold"> 
            <Column>
              <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} className="SetWritingHold"/>
            </Column>
          </div>
          </PageLayout>
      </div>
    )
  }